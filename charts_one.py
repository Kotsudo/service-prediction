import src.helpers.data_provider as dp
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import numpy as np


frames = list()

X, y = dp.graph_dijkstra_frame()
frames.append({"data": y, "title": "realizującej algorytm Dijkstry"})

for frame in frames:
    density, bins, _ = plt.hist(frame['data'], density=True, bins=20)
    count, _ = np.histogram(frame['data'], bins)
    for x, y, num in zip(bins, density, count):
        if num != 0:
            plt.text(x, y + 100, num, fontsize=12, rotation=-90)  # x,y,str
    plt.hist(frame['data'], bins=20, alpha=0.5,
             histtype='stepfilled', color='steelblue',
             edgecolor='none')

    plt.xlabel('Czas wykonania żądania [s]')
    plt.ylabel('Liczba pomiarów')
    plt.title('Histogram czasu wykonania żądania dla usługi ' + frame['title'])
    plt.show()


    plt.plot(frame['data'], X['num_of_nodes'], 'o', color='black')
    plt.show()

    plt.plot(frame['data'], X['num_of_edges'], 'o', color='blue')
    plt.show()



