import src.helpers.data_provider as dp
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import numpy as np


frames = list()

X, y = dp.sum_numbers_frame()
frames.append({"data": y, "title": "sumowania liczb"})

X, y = dp.sum_array_frame()
frames.append({"data": y, "title": "sumowania wartości w tablicy"})

X, y = dp.array_search_frame()
frames.append({"data": y, "title": "przeszukiwania tablicy"})

X, y = dp.bubble_sort_frame()
frames.append({"data": y, "title": "sortowania bąbelkowego"})

X, y = dp.prime_factors_frame()
frames.append({"data": y, "title": "rozkładu liczb na czynniki pierwsze"})

X, y = dp.hash_cracker_frame()
frames.append({"data": y, "title": "łamania haseł"})

X, y = dp.graph_colouring_frame()
frames.append({"data": y, "title": "kolorowania grafu"})

X, y = dp.graph_kruskal_frame()
frames.append({"data": y, "title": "realizującej algorytm Kruskala"})

X, y = dp.graph_dijkstra_frame()
frames.append({"data": y, "title": "realizującej algorytm Dijkstry"})


for frame in frames:
    density, bins, _ = plt.hist(frame['data'], density=True, bins=20)
    count, _ = np.histogram(frame['data'], bins)
    for x, y, num in zip(bins, density, count):
        if num != 0:
            plt.text(x, y + 10, num, fontsize=12, rotation=-90)  # x,y,str
    plt.hist(frame['data'], bins=30, alpha=0.5,
             histtype='stepfilled', color='steelblue',
             edgecolor='none')

    plt.xlabel('Czas wykonania żądania [s]')
    plt.ylabel('Liczba pomiarów')
    plt.title('Histogram czasu wykonania żądania dla usługi ' + frame['title'])
    plt.show()



