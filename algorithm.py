import src.helpers.algorithm as model_selection


filename = 'input/bubble_sort.csv'
label = 'response_time'
drop = ["date", "time", "response_memory_round", "response_memory", "response_time_round"]
error_score_type='percentage'

algorithm = model_selection.Algorithm(filename=filename, label=label, drop=drop, error_score_type=error_score_type)

model, mae, mape, features = algorithm.run()

print(model, mae, mape, features)
