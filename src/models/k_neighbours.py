from sklearn.neighbors import KNeighborsRegressor
from src.models.model import Model

class KNeighbours(Model):
    def __init__(self, n_neighbors=5):
        self.n_neighbours=n_neighbors

    def get_base_model(self):
        return KNeighborsRegressor(n_neighbors=self.n_neighbours)