from sklearn.ensemble import RandomForestRegressor
from src.models.model import Model

class RandomForest(Model):
    def get_base_model(self):
        return RandomForestRegressor()