from sklearn.tree import DecisionTreeRegressor
from src.models.model import Model

class DecisionTree(Model):
    def get_base_model(self):
        return DecisionTreeRegressor()