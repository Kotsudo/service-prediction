from sklearn.linear_model import LinearRegression as LR
from src.models.model import Model

class LinearRegression(Model):
    def get_base_model(self):
        return LR()