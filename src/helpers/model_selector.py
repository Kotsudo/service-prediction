import src.models.k_neighbours as kn
import src.models.linear_regression as lr
import src.models.decision_tree as dt
import src.models.random_forest as rf
import src.models.neural_network as nn

class ModelSelector:
    def __init__(self, x_train, x_test, y_train, y_test):
        self.x_train = x_train
        self.x_test = x_test
        self.y_train = y_train
        self.y_test = y_test
        self.model = None
        self.mae = 999999
        self.mape = 999999

    def get_best_model(self, error_score='percentage'):
        for base_model in self._get_models():
            print(base_model['name'])
            mae, mape = base_model['model'].run_model(self.x_train, self.x_test, self.y_train, self.y_test)
            if self._is_better(mae, mape, error_score):
                self.model = base_model
                self.mae = mae
                self.mape = mape

        print('Model found!', self.model['name'], self.mae, self.mape)
        return self.model, self.mae, self.mape

    def _is_better(self, mae, mape, error_score):
        if error_score == 'percentage':
            return self.mape > mape

        return self.mae > mae

    def _get_models(self):
        return [
            {'name': 'K-neighbours', 'model': kn.KNeighbours(n_neighbors=5)},
            {'name': 'Linear regression', 'model': lr.LinearRegression()},
            {'name': 'Decision tree', 'model': dt.DecisionTree()},
            {'name': 'Random forest', 'model': rf.RandomForest()},
            {'name': 'Neural network, relu, 1 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100,))},
            {'name': 'Neural network, relu, 2 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100,100,))},
            {'name': 'Neural network, relu, 3 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100,100,100))},
            {'name': 'Neural network, relu, 4 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100,100,100,100,))},
            {'name': 'Neural network, relu, 5 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100,100,100,100,100,))},
            {'name': 'Neural network, logistic, 1 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100,), activation='logistic')},
            {'name': 'Neural network, logistic, 2 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100, 100,), activation='logistic')},
            {'name': 'Neural network, logistic, 3 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100, 100,100,), activation='logistic')},
            {'name': 'Neural network, logistic, 4 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100, 100,100,100,), activation='logistic')},
            {'name': 'Neural network, logistic, 5 layers, 100 neurons', 'model': nn.NeuralNetwork(hidden_layer_sizes=(100, 100,100,100,100,), activation='logistic')},
        ]
