import pandas as pd
from sklearn import preprocessing

def sum_numbers_frame():
    df = _read("input/sum_numbers.csv")
    df.a = df.a.astype(int)
    df.b = df.b.astype(int)
    df = df[df.response_time < 0.3]
    return _frame_return(df)

def sum_array_frame():
    df = _read("input/sum_array.csv")
    df.array_size = df.array_size.astype(int)
    df = df[df.response_time < 31]
    return _frame_return(df)

def array_search_frame():
    df = _read("input/array_search.csv")
    df.array_size = df.array_size.astype(int)
    df = df[df.response_time < 24]
    return _frame_return(df)

def bubble_sort_frame():
    df = _read("input/bubble_sort.csv")
    df.array_size = df.array_size.astype(int)
    df = df[df.response_time < 26]
    return _frame_return(df)

def prime_factors_frame():
    df = _read("input/prime_factors.csv", converters={'n':int})
    df['divisible_by_two'] = df.apply(lambda row: (int(row.n % 2 == 0)), axis=1)
    df['divisible_by_three'] = df.apply(lambda row: (int(row.n % 3 == 0)), axis=1)
    df['divisible_by_five'] = df.apply(lambda row: (int(row.n % 5 == 0)), axis=1)
    df['divisible_by_seven'] = df.apply(lambda row: (int(row.n % 7 == 0)), axis=1)
    df['divisible_by_eleven'] = df.apply(lambda row: (int(row.n % 11 == 0)), axis=1)
    df['grater_than_one'] = df.apply(lambda row: (int(row.n > 399999999999999)), axis=1)

    df = df[df.response_time < 45]
    return _frame_return(df)

def hash_cracker_frame():
    df = _read("input/hash_cracker.csv")
    df.hash = df.hash.astype(str)
    df.hash_type = df.hash_type.astype(str)

    le_hash = preprocessing.LabelEncoder()
    le_hash.fit(df.hash)
    df.hash = le_hash.transform(df.hash)

    le_hash_type = preprocessing.LabelEncoder()
    le_hash_type.fit(df.hash_type)
    df.hash_type = le_hash_type.transform(df.hash_type)

    df = df[df.response_time < 0.4]
    return _frame_return(df)

def graph_colouring_frame():
    df = _read("input/graph_colouring.csv")
    df.num_of_nodes = df.num_of_nodes.astype(int)
    df.num_of_edges = df.num_of_edges.astype(int)
    df.colors = df.colors.astype(int)
    df = df[df.response_time < 20]
    return _frame_return(df)

def graph_kruskal_frame():
    df = _read("input/graph_kruskal.csv")
    df.num_of_nodes = df.num_of_nodes.astype(int)
    df.num_of_edges = df.num_of_edges.astype(int)
    return _frame_return(df)

def graph_dijkstra_frame():
    df = _read("input/graph_dijkstra.csv")
    df.num_of_nodes = df.num_of_nodes.astype(int)
    df.num_of_edges = df.num_of_edges.astype(int)
    df.start = df.start.astype(int)
    df = df[df.response_time < 42]
    #df = df.drop(df[df.response_time > 30].index)
    return _frame_return(df)

def _change_types(df):
    df.dropna(inplace=True)
    df.drop(["date", "time", "response_memory_round", "response_memory", "response_time_round"], axis=1, inplace=True)
    df.hour = df.hour.astype(int)
    df.minute = df.minute.astype(int)
    df.second = df.second.astype(int)
    df.week_day = df.week_day.astype(int)

    df['response_time'] = df['response_time'].str.replace(',', '.')
    df.response_time = df.response_time.astype(float)

    return df


def _frame_return(df):
    y = df["response_time"]
    df.drop(["response_time"], axis=1, inplace=True)

    return df, y

def _read(filename, converters=None):
    df = pd.read_csv(filename, sep=";", error_bad_lines=False, converters=converters)
    return _change_types(df)