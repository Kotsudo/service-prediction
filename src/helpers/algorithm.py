import src.helpers.data_preparator as dp
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from src.feature_selection.sfs import SfsSelector
import src.helpers.model_selector as ms

class Algorithm:
    def __init__(self, filename, label, drop, error_score_type='percentage'):
        self.error_score_type=error_score_type
        self.X, self.y = dp.prepare_frames(filename, label, drop)

        self.model = None
        self.mae = 999999
        self.mape = 999999
        self.features = []

    def run(self):
        for i in range(1, len(self.X.columns) + 1):
            selector = SfsSelector(x=self.X, y=self.y, k_features=i)
            x_selected = selector.get_transformed_data()
            x_train, x_test, y_train, y_test = train_test_split(x_selected, self.y, test_size=0.2, random_state=123)

            scaler = StandardScaler()
            train_scaled = scaler.fit_transform(x_train)
            test_scaled = scaler.transform(x_test)

            model_selector = ms.ModelSelector(train_scaled, test_scaled, y_train, y_test)
            model, mae, mape = model_selector.get_best_model(error_score=self.error_score_type)

            if self._is_better(mae, mape):
                self.model = model
                self.mae = mae
                self.mape = mape
                self.features = selector.get_column_names()

        return self.model, self.mae, self.mape, self.features


    def _is_better(self, mae, mape):
        if self.error_score_type == 'percentage':
            return self.mape > mape

        return self.mae > mae