from mlxtend.feature_selection import SequentialFeatureSelector as SFS
from sklearn.linear_model import LinearRegression
import pandas as pd

class SfsSelector:
    def __init__(self, x, y, k_features=3):
        self.x = x
        self.y = y
        self.k_features = k_features
        self.sfs = SFS(LinearRegression(), k_features=k_features,forward=True,floating=False,scoring='r2',cv=0)\
            .fit(self.x, self.y)

    def get_column_names(self):
        sfs_features = self.sfs.subsets_[self.k_features]['feature_names']
        sfs_most_important_columns = [sfs_features[i] for i in range(len(sfs_features))]
        return sfs_most_important_columns

    def get_transformed_data(self):
        names = self.get_column_names()
        return pd.DataFrame(self.sfs.transform(self.x), columns=names)