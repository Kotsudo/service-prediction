from sklearn.decomposition import PCA
from sklearn.linear_model import LinearRegression
import pandas as pd

class PcaSelector:
    def __init__(self, x, y, k_features=3):
        self.x = x
        self.y = y
        self.k_features = k_features
        self.pca = PCA(n_components=k_features)\
            .fit(self.x, self.y)


    def get_transformed_data(self):
        return self.pca.transform(self.x)