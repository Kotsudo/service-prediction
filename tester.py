from ortools.linear_solver import pywraplp
import src.helpers.data_provider as dp
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import src.models.neural_network as nn

X, y = dp.graph_dijkstra_frame()
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=123)

scaler = StandardScaler()
train_scaled = scaler.fit_transform(X_train)
test_scaled = scaler.transform(X_test)
print(y.describe())


def network(x1, x2, x3):
    l = list()
    for i in range(0, int(x3.SolutionValue())):
        l.append(x2.SolutionValue())

    nn_model = nn.NeuralNetwork(max_iter=x1.SolutionValue(), hidden_layer_sizes=tuple(l))
    mse, mae, test_mse, test_mae = nn_model.run_model(train_scaled, test_scaled, y_train, y_test)

    return test_mae


def main():
    solver = pywraplp.Solver(
        'SolveMIP', pywraplp.Solver.CBC_MIXED_INTEGER_PROGRAMMING
    )

    infinity = solver.infinity()
    x1 = solver.IntVar(0.0, infinity, 'x1')
    x2 = solver.IntVar(0.0, infinity, 'x2')
    x3 = solver.IntVar(0.0, infinity, 'x3')

    solver.Add(x1 <= 500)
    solver.Add(x1 >= 200)
    solver.Add(x2 >= 100)
    solver.Add(x2 <= 150)
    solver.Add(x3 >= 1)
    solver.Add(x3 <= 5)
    solver.Maximize(network(x1, x2, x3))

    status = solver.Solve()
    if status == pywraplp.Solver.OPTIMAL:
        print('Solution:')
        print('Objective value =', solver.Objective().Value())
        print('x1 =', x1.solution_value())
        print('x2 =', x2.solution_value())
        print('x3 =', x3.solution_value())
    else:
        print('The problem does not have an optimal solution.')

    print('\nAdvanced usage:')
    print('Problem solved in %f milliseconds' % solver.wall_time())
    print('Problem solved in %d iterations' % solver.iterations())
    print('Problem solved in %d branch-and-bound nodes' % solver.nodes())


if __name__ == '__main__':
    main()
